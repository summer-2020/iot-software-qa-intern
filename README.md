
## Project

### `/edge` folder

[python-sdk]: https://github.com/ClearBlade/ClearBlade-Python-SDK
[psutil]: https://pypi.org/project/psutil/
[dotenv]: https://pypi.org/project/python-dotenv/

Contains the code for the "device" that is generating the metrics. As
per the recommendation, this code uses the [ClearBlade Python SDK][python-sdk],
the [psutil][psutil] library for system monitoring, and [dotenv][dotenv] for
passing the keys and secrets to the program.

#### Running

Create a `.env` file that contains:

```
CLEARBLADE_SYSTEM_KEY=...
CLEARBLADE_SYSTEM_SECRET=...
CLEARBLADE_DEVICE_NAME=...
CLEARBLADE_DEVICE_KEY=...
```

Make sure to replace `...` with the appropriate values.
To run the script that generates data:

```bash
python -m venv venv
source ./venv/bin/activate[.sh,.fish]
python ./edge/main.py
```

### `/code` folder

Contains the source of the *code services*(s) used in the coding challenge.

## System

The following resources were created in the *ClearBlade System*:

### Topics

* `metrics`: Holds the real-time metrics data from the edge device.

* `analytics`: Holds the analytic data comming from the `doCpuAnalytics` *code service*.

### Collections

* `metrics-collection`: Holds the real-time data persistently.

### Roles

* `Metrics`: Provides access to the mentioned *topics* and *collections*.

### Code services

* `onMetricsPublish`: Runs each time a new message is published to the `metrics` topic.
  It publishes the metrics to the `metrics-collection`.

* `doMetricsAnalytics`: Runs with a timer. Reads the `metrics-collection` and publishes
  simple moving analytics to the `analytics` topic.

## Possible improvements

[arrow]: https://arrow.apache.org/

* Send alerts using email or other notification API(s).

* Use a message encoding that is faster to transport and decode (e.g.
  [Apache arrow][arrow]), increasing the throughput when ingesting a
  lot of data.

