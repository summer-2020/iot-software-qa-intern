/**
 * Type: Micro Service
 * Description: Grabs newest entries from given collection and calculates simple moving analytics.
 * @param {CbServer.BasicReq} req
 * @param {string} req.systemKey
 * @param {string} req.systemSecret
 * @param {string} req.userEmail
 * @param {string} req.userid
 * @param {string} req.userToken
 * @param {boolean} req.isLogging
 * @param {[id: string]} req.params
 * @param {CbServer.Resp} resp
 */


// Fetch / Publish parameters
const FETCH_SINCE = 60;                  // fetch entries since given seconds
const PULL_COLL = 'metrics-collection';  // pull from this collection
const PUSH_TOPIC = 'analytics';          // publish to this topic


// Columns that will be ignored during analytics
const columnsToIgnore = [
    'item_id',
    'time',    
];


// Thresholds
const columnThresholds = {
    'cpu': 70,
};


// Math related
// ----------------


function isNumber(x) {
    return !isNaN(x);
}


function mean(arr) {
    return arr.reduce(function(a, b) { return a + b }, 0) / arr.length;
}


function sd(arr) {
    var m = mean(arr);
    var arrSquared = arr.map(function(x) { return Math.pow(x - m, 2) });
    var arrSquaredAverage = arrSquared.reduce(function(a, b) { return a + b }, 0) / (arr.length - 1);
    return Math.sqrt(arrSquaredAverage);
}


function min(arr) {
    return Math.min.apply(Math, arr);
}


function max(arr) {
    return Math.max.apply(Math, arr);
}


// Entrypoint
// ----------------


function doMetricsAnalytics(req,resp){
    ClearBlade.init({ request: req });

    // constructs Date object AVERAGE_SINCE seconds in the past
    var sinceTime = new Date();
    sinceTime.setSeconds(sinceTime.getSeconds() - FETCH_SINCE);
    
    // creates query that gets all entries from last AVERAGE_SINCE seconds
    var query = ClearBlade.Query();
    query.greaterThanEqualTo('time', sinceTime);

    // fetch entries
    var coll = ClearBlade.Collection({ collectionName: PULL_COLL });
    coll.fetch(query, function(err, data) {
        if (err) {
            var errmsg = 'error fetching data: ' + JSON.stringify(data);
            log(errmsg);
            resp.error(errmsg);
        } else {

            // return if we have no data
            if (data.DATA.length <= 0) {
                var msg = "no items to process";
                log(msg);
                resp.success(msg);
                return;
            }

            // analyses fetched data
            var result = analyze(data.DATA);
            thresholdCheck(result);

            // publishes result to push topic
            var mqtt = ClearBlade.Messaging();
            mqtt.publish(PUSH_TOPIC, JSON.stringify(result));
            resp.success(result);
        }
    });

    // analyze calculates simple statistics for the given rows of data.
    function analyze(data) {
        var result = {};

        // iterate the keys (based on the first element of the data)
        var first = data[0];
        Object.keys(first).forEach(function(key) {

            // skip key if it should be ignored
            if (columnsToIgnore.indexOf(key) >= 0) {
                return;
            }

            // item is processed different if it is a number
            if (isNumber(first[key])) {
                var arr = data.map(function(x) { return x[key]; });
                result[key + '_MEAN'] = mean(arr);
                result[key + '_SD'] = sd(arr);
                result[key + '_MIN'] = min(arr);
                result[key + '_MAX'] = max(arr);
            } else {
                result[key] = first[key];
            }
        });

        return result;
    }

    // thresholdCheck sends an alert if any of the items in the analyzed result exceeds a threshold
    // value.
    function thresholdCheck(result) {
        Object.keys(columnThresholds).forEach(function(key) {

            var mean_key = key + '_MEAN';

            // skip key threshold check if mean key is not present in result
            if (Object.keys(result).indexOf(mean_key) < 0) {
                return;
            }

            // compare mean key value against configured threshold
            var value = result[mean_key];
            var threshold = columnThresholds[key];
            if (value >= threshold) {
                log("value of '" + key + "' exceeded threshold of " + threshold)
                // TODO: Send alert
            }
        });
    }
}
