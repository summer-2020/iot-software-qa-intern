/**
 * Type: Micro Service
 * Description: Pulls messages from given topic and pushes them into a collection.
 * @param {CbServer.BasicReq} req
 * @param {string} req.systemKey
 * @param {string} req.systemSecret
 * @param {string} req.userEmail
 * @param {string} req.userid
 * @param {string} req.userToken
 * @param {boolean} req.isLogging
 * @param {[id: string]} req.params
 * @param {CbServer.Resp} resp
 */


// Pull parameters
const PUSH_COLL = 'metrics-collection';


// Columns that will be pushed to the collection
const columnsToCollect = [
    'hostname',
    'cpu',
    'memory',
    'memory_available',
    'memory_used',
];


// Entrypoint
// ----------------


function onMetricsPublish(req,resp){
    ClearBlade.init({ request: req });

    // parses message body
    var metrics = {};
    try {
        metrics = JSON.parse(req.params.body);
    } catch (err) {
        var errmsg = "could not parse request body: " + err;
        log(errmsg);
        resp.error(errmsg);
        return;
    }

    // checks that we have required columns
    for (var idx = 0; idx < columnsToCollect.length; idx++) {
        if (!(columnsToCollect[idx] in metrics)) {
            var errmsg = "metric '" + columnsToCollect[idx] + "' not found in request body";
            log(errmsg);
            resp.error(errmsg);
            return;
        }
    }

    // creates our new entry in the collection
    var newEntry = { time: new Date() };
    columnsToCollect.forEach(function(col) {
        newEntry[col] = metrics[col];
    });

    // inserts new intry
    var coll = ClearBlade.Collection({ collectionName: PUSH_COLL });
    coll.create(newEntry, function(err, data) {
        if (err) {
            var errmsg = "error creating collection entry: " + JSON.stringify(data);
            log(errmsg);
            resp.error(errmsg);
        } else {
            resp.success(data);
        }
    });
}
