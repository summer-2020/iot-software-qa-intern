import json
import os
import socket
import time

import psutil
from dotenv import load_dotenv
load_dotenv()
from clearblade.ClearBladeCore import System, Query, Developer


# Publish parameters
PUSH_TOPIC = 'metrics' # send message to `cpu` topic
PUSH_INTERVAL = 1  # send mesage every second

# ClearBlade credentials
SYSTEM_KEY = os.getenv("CLEARBLADE_SYSTEM_KEY")
SYSTEM_SECRET = os.getenv("CLEARBLADE_SYSTEM_SECRET")
DEVICE_NAME = os.getenv("CLEARBLADE_DEVICE_NAME")
DEVICE_KEY = os.getenv("CLEARBLADE_DEVICE_KEY")


def collect_system_metrics():
    """
    collect_cpu_metrics returns a dictionary with global CPU (and per-cpu) usage
    percents.
    """

    memory = psutil.virtual_memory()

    return {
        'hostname': socket.gethostname(),
        'cpu': psutil.cpu_percent(),
        'memory': memory.total,
        'memory_available': memory.available,
        'memory_used': memory.total - memory.available,
    }


def main():

    # connects to system and logs in
    system = System(SYSTEM_KEY, SYSTEM_SECRET)
    device = system.Device(DEVICE_NAME, DEVICE_KEY)

    # creates mqtt client
    mqtt = system.Messaging(device)
    mqtt.connect()

    try:
        while True:
            system_metrics = collect_system_metrics()
            mqtt.publish(PUSH_TOPIC, json.dumps(system_metrics))
            time.sleep(PUSH_INTERVAL)

    except KeyboardInterrupt:
        mqtt.disconnect()


if __name__ == '__main__':
    main()